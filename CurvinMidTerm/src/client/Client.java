package client;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

import blackjack_contract.Card;
import blackjack_contract.Message;

/**
 *
 * @author Chad Curvin
 */
public class Client implements Runnable {
		private static final String username = "CCurvin";
            Socket s = null;
        private Client(Socket s) {
                this.s = s;
        }

        @Override
        public void run() {
                DataInputStream dis;
                try {
                        dis = new DataInputStream(s.getInputStream());
                        while (s.isConnected() && !s.isClosed()) {
                                String test = dis.readUTF();
                                System.out.println("Client " + test);
                        }
                } catch (IOException e) {
                }
        }
        
        public static void Display_GUI() {
            ChatPanel win = new ChatPanel();
            win.setVisible(true);
        }
        
        public static void Start_Game() {
        	String started = "Game is started.";        	
        	System.out.println(started);

        	Message.createStartMessage(started);        	
        }
        
        public static void Send_Uname(String username, Socket s) throws IOException {
        	ObjectOutputStream os = new ObjectOutputStream(s.getOutputStream());
            os.writeUTF(username);
            //dos.writeUTF("CCurvin");
            os.flush();
        }
        
        public static String Send_Chat() {
        	String val = "I am a chat message";
        	return val;
        }
        
        public static void Get_Server_Conn_Message() {
            String ack = "ACKNOWLEDGE";
            String nack = "DENY";
            String succ = "Connection Successful";
            
            System.out.print("Ack message is: ");
            System.out.println(Message.getAcknowledgeMessage());
            System.out.print("Deny message is: ");
            System.out.println(Message.getDenyMessage());
            
            if (Message.getAcknowledgeMessage().equals(ack)) {
            	System.out.println(succ);
            } else if (Message.getAcknowledgeMessage().equals(nack)) {
            	System.out.println("Incorrect username");
            } else {
            	System.out.println("Yep. Probably ack'd...");
            }
        }
        
        public Card Get_Card() {
        	Card card = new Card(Card.Suite.HEARTS, Card.Value.QUEEN);
        	return card;
        }
        
        public Card Hit() {
        	return Get_Card();
        }
        
        public void Stay() {
        	Card.Value val = Card.Value.SIX;
        	System.out.println(val);
        }
        
        public static Object Get_Server_Message() {
        	Object s = Message.createChatMessage("How are you doing?", username);
			return s;
        }
        
        public static void main(String[] args) throws UnknownHostException, IOException {
                Socket s = new Socket("localhost", 8989);                    

                new Thread(new Client(s)).start();
                
                Display_GUI();                
                
                Send_Uname(username, s);
                Get_Server_Conn_Message();

                System.out.println(Message.createChatMessage(Send_Chat(), username).getText());
                                
                Start_Game();                       
                
                boolean play = true;
                int num = 22; //Dummy var for if statement to make it compile
                while (play) {
                	if(num == 22) { //Card Value total > 21
                		//Send message to server BUST
                		System.out.println(Message.createChatMessage("BUST", username));
                	} else if(num < 21) { //Card Value total > everyone else's Card Value total
                		//Send message to server WIN
                	} else {
                		System.out.println("PUSH");
                	}
                	play = false;
                }

                s.close(); //close out the socket
                //Add message to show it closed
                System.out.println("Socket closed. Game is over.");
                System.exit(0); //exit the program                                     
        }
}
